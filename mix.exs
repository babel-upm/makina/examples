defmodule Examples.MixProject do
  use Mix.Project

  def project do
    [
      apps_path: "examples",
      version: "0.1.0",
      start_permanent: Mix.env() == :prod,
      deps: []
    ]
  end
end
