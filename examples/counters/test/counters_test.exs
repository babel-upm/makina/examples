defmodule CountersTest do
  use PropCheck
  use ExUnit.Case
  import PropCheck.StateM

  property "CountersModel.Generators" do
    check_model(CountersModel.Generators)
  end

  property "CountersModel.Checks" do
    check_model(CountersModel.Checks)
  end

  defp check_model(model) do
    forall cmds <- commands(model) do
      r = {_, _, result} = run_commands(model, cmds)

      (result == :ok)
      |> when_fail(print_report(r, cmds))
      |> aggregate(command_names(cmds))
    end
  end
end
