defmodule CountersModel.Generators do
  use Makina, implemented_by: Counters

  state counters: [] :: [symbolic(pid)]

  invariants counters_list: is_list(counters)

  command new() :: pid() do
    next counters: [result | state.counters]
  end

  command put(counter :: symbolic(pid()), value :: integer()) :: :ok do
    pre counters != []
    args value: nat(), counter: oneof(counters)
  end

  command get(counter :: symbolic(pid())) :: integer() do
    pre counters != []
    args counter: oneof(counters)
  end
end
