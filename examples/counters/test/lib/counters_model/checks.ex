defmodule CountersModel.Checks do
  use Makina, extends: CountersModel.Generators

  state values: %{} :: %{symbolic(pid) => integer()} | %{}

  invariants constant_size: length(Map.keys(values)) == length(counters)

  command new() :: pid() do
    next values: Map.put(values, result, 0)
  end

  command put() :: :ok do
    next super() ++ [values: Map.put(values, counter, value)]
  end

  command get() :: integer() do
    post result == values[counter]
  end
end
