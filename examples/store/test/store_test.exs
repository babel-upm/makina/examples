defmodule StoreTest do
  use PropCheck
  use ExUnit.Case
  import PropCheck.StateM

  property "StoreModelFunctionalStyle" do
    check_model(StoreModelFunctionalStyle)
  end

  property "StoreModel" do
    check_model(StoreModel)
  end

  property "StoreModelExt.Generators" do
    check_model(StoreModelExt.Generators)
  end

  property "StoreModelExt.Judge" do
    check_model(StoreModelExt.Judge)
  end

  property "StoreModelComp.Judge" do
    check_model(StoreModelComp.Judge)
  end

  defp check_model(model) do
    forall cmds <- commands(model) do
      r = {_, _, result} = run_commands(model, cmds)

      (result == :ok)
      |> when_fail(print_report(r, cmds))
      |> aggregate(command_names(cmds))
    end
  end
end
