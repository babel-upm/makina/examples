defmodule StoreModelFunctionalStyle do
  import PropCheck.BasicTypes
  
  def initial_state, do: %{}

  def command(state) do
    oneof([
      {:call, Store, :new, []}
      | if map_size(state) > 0 do
          [
            {:call, Store, :get, [oneof(Map.keys(state))]},
            {:call, Store, :put, [oneof(Map.keys(state)), integer()]}
          ]
        else
          []
        end
    ])
  end

  def precondition(state, call) do
    case call do
      {:call, _, :get, [store]} -> Map.has_key?(state, store)
      {:call, _, :put, [store, _]} -> Map.has_key?(state, store)
      _ -> true
    end
  end

  def next_state(state, result, call) do
    case call do
      {:call, _, :new, _} -> Map.put(state, result, 0)
      {:call, _, :put, [store, value]} -> Map.put(state, store, value)
      _ -> state
    end
  end

  def postcondition(state, call, result) do
    case call do
      {:call, _, :get, [store]} -> Map.get(state, store) == result
      _ -> true
    end
  end
end
