defmodule StoreModelExt.Judge do
  use Makina, extends: StoreModelExt.Generators

  state values: %{} :: %{symbolic(Store.store()) => integer()} | %{}

  command new() :: Store.store() do
    next values: Map.put(values, result, 0)
  end

  command get() :: integer() do
    post Map.get(values, store) == result
  end

  command put() :: {:set, integer()} do
    next values: Map.put(values, store, value)
  end
end
