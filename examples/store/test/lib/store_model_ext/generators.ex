defmodule StoreModelExt.Generators do
  use Makina, implemented_by: Store

  state stores: [] :: [symbolic(Store.store())]

  command new() :: Store.store() do
    next stores: [result | stores]
  end

  command get(store :: symbolic(Store.store())) :: integer() do
    pre stores != []
    args store: oneof(stores)
  end

  command put(store :: symbolic(Store.store()), value :: integer()) :: {:set, integer()} do
    pre stores != []
    args store: oneof(stores), value: nat()
  end
end
