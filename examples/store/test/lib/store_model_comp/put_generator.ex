defmodule StoreModelComp.PutGenerator do
  use Makina, implemented_by: Store

  state stores: [] :: [symbolic(Store.store())]

  command put(store :: symbolic(Store.store()), value :: integer()) :: {:set, integer()} do
    pre stores != []
    args store: oneof(stores), value: nat()
  end
end
