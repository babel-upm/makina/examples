defmodule StoreModelComp.NewGenerator do
  use Makina, implemented_by: Store

  state stores: [] :: [symbolic(Store.store())]

  command new() :: Store.store() do
    next stores: [result | stores]
  end
end
