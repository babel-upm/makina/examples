defmodule StoreModelComp.Judge do
  use Makina,
    extends: [
      StoreModelComp.NewGenerator,
      StoreModelComp.PutGenerator,
      StoreModelComp.GetGenerator
    ]

  state values: %{} :: %{symbolic(Store.store()) => integer()} | %{}

  command new() :: Store.store() do
    next [values: Map.put(values, result, 0)]
  end

  command get(store :: Store.store()) :: integer do
    post Map.get(values, store) == result
  end

  command put(store :: Store.store(), value :: integer) :: integer do
    next [values: Map.put(values, store, value)]
  end
end
