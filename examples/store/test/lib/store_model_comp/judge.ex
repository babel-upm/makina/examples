defmodule StoreModelComp.Judge do
  use Makina,
    extends: [
      StoreModelComp.NewGenerator,
      StoreModelComp.PutGenerator,
      StoreModelComp.GetGenerator
    ]

  state values: %{} :: %{symbolic(Store.store()) => integer()} | %{}

  command new() do
    next [values: Map.put(values, result, 0)]
  end

  command get() do
    post Map.get(values, store) == result
  end

  command put(value :: integer()) do
    next [values: Map.put(values, store, value)]
  end
end
