defmodule StoreModelComp.GetGenerator do
  use Makina, implemented_by: Store

  state stores: [] :: [symbolic(Store.store())]

  command get(store :: symbolic(Store.store())) :: integer do
    pre stores != []
    args store: oneof(stores)
  end
end
