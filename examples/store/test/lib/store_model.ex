defmodule StoreModel do
  use Makina, implemented_by: Store

  state stores: %{} :: %{symbolic(Store.store()) => integer()} | %{}

  command new() :: Store.store() do
    next stores: Map.put(stores, result, 0)
  end

  command put(store :: symbolic(Store.store()), value :: integer()) :: {:set, integer()} do
    pre map_size(stores) > 0
    args store: oneof(Map.keys(stores)), value: integer()
    next stores: Map.put(stores, store, value)
  end

  command get(store :: symbolic(Store.store())) :: integer do
    pre map_size(stores) > 0
    args store: oneof(Map.keys(stores))
    post Map.get(stores, store) == result
  end
end
