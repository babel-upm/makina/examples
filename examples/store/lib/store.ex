defmodule Store do
  @type store :: pid()

  def new do
    spawn fn -> value 0 end
  end

  def put(c,n) do
    send c, {:set, n}
  end

  def get c do
    send c, {:get, self()}
    receive do
      value -> value
    end
  end

  def value n do
    receive do
      {:set, m} ->
        value m
      {:get, pid} ->
          send pid, n
          value n
    end
  end
end
