defmodule FizzBuzz do
  use Agent

  defmodule Actions do
    def print_fizzbuzz, do: IO.puts("fizzbuzz")
    def print_fizz, do: IO.puts("fizz")
    def print_buzz, do: IO.puts("buzz")
    def print_digit(digit), do: IO.puts(digit)
  end

  def start(initial_value) do
    Agent.start_link(fn -> initial_value end, name: __MODULE__)
  end

  def increment() do
    state = Agent.get(__MODULE__, & &1)
    Agent.update(__MODULE__, &(&1 + 1))

    cond do
      rem(state, 3) == 0 and rem(state, 5) == 0 -> Actions.print_fizzbuzz()
      rem(state, 3) == 0 -> Actions.print_fizz()
      rem(state, 5) == 0 -> Actions.print_buzz()
      true -> Actions.print_digit(state)
    end

    :ok
  end
end
