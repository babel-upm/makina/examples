# based on the example in https://statecharts.dev/fizzbuzz-actions-guards.html
defmodule FizzBuzzModel do
  import ExUnit.CaptureIO, only: [capture_io: 1]
  use Makina, implemented_by: FizzBuzz

  state i: 0,
        prev: "fizzbuzz\n",
        # state labels
        fizz: true,
        buzz: true,
        fizzbuzz: true,
        digit: false

  invariants valid_state: fizz or buzz or fizzbuzz or digit

  # labels
  def digit(state), do: not fizz(state) and not buzz(state) and not fizzbuzz(state)
  def fizz(state), do: rem(state.i, 3) == 0
  def buzz(state), do: rem(state.i, 5) == 0
  def fizzbuzz(state), do: fizz(state) and buzz(state)

  command increment() do
    call capture_io(fn -> super() end)

    next i: i + 1, prev: result

    next super() ++
           [
             fizz: fn _ -> fizz(state) end,
             buzz: fn _ -> buzz(state) end,
             fizzbuzz: fn _ -> fizzbuzz(state) end,
             digit: fn _ -> digit(state) end
           ]

    post do
      cond do
        fizzbuzz -> prev == "fizzbuzz\n"
        fizz -> prev == "fizz\n"
        buzz -> prev == "buzz\n"
        digit -> prev == "#{i - 1}\n"
      end
    end
  end
end

# based on the example in https://statecharts.dev/fizzbuzz-actions-guards.html
##
## data labels (maybe view?)
# defmodule DigitFizzBuzzFizzbuzz do
#   import ExUnit.CaptureIO, only: [capture_io: 1]
#   use Makina, implemented_by: FizzBuzz

#   state i: 0

#   statelabels
#     fizz: rem(i, 3) == 0,
#     buzz: rem(i, 5) == 0,
#     fizzbuzz: fizz and buzz,
#     digit: not fizz and not buzz and not fizzbuzz

#   invariants valid_state: fizz or buzz or fizzbuzz or digit

#   command increment() do
#     call capture_io(fn -> super() end)
#     next i: i + 1

#     post do
#       cond do
#         fizzbuzz(state) -> result == "fizzbuzz\n"
#         digit(state) -> result == "#{i}\n"
#         fizz -> result == "fizz\n"
#         buzz -> result == "buzz\n"
#       end
#     end
#   end
# end
