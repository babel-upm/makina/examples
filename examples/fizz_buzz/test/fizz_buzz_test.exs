defmodule FizzBuzzTest do
  use ExUnit.Case
  use PropCheck
  import PropCheck.StateM

  @tag :fizzbuzz
  property "FizzBuzzModel" do
    forall cmds <- commands(FizzBuzzModel) do
      FizzBuzz.start(0)
      r = {_, _, result} = run_commands(FizzBuzzModel, cmds)
      Agent.stop(FizzBuzz)

      (result == :ok)
      |> when_fail(print_report(r, cmds))
      |> aggregate(command_names(cmds))
    end
  end
end
