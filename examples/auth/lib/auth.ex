defmodule Auth do
  require Agent

  @opaque server() :: pid()

  @spec start() :: :ok
  def start() do
    {:ok, _} = Agent.start_link(fn -> %{passwords: %{}, tokens: %{}} end, name: __MODULE__)
    :ok
  end

  @spec stop() :: :ok
  def stop() do
    :ok = Agent.stop(__MODULE__)
    :ok
  end

  @type user() :: [integer()]
  @type pass() :: [integer()]
  @type token() :: pos_integer()

  @spec reg(user, pass) :: :ok | :error
  def reg(user, pass) do
    Agent.get_and_update(__MODULE__, fn state ->
      if user in Map.keys(state.passwords) do
        {:error, state}
      else
        state = %{state | passwords: Map.put(state.passwords, user, pass)}
        {:ok, state}
      end
    end)
  end

  @spec gen(user, pass) :: {:ok, token} | :error
  def gen(user, pass) do
    Agent.get_and_update(__MODULE__, fn state ->
      if {user, pass} in state.passwords do
        token = Map.keys(state.tokens) |> gen_token()
        state = %{state | tokens: Map.put(state.tokens, token, user)}
        {{:ok, token}, state}
      else
        {:error, state}
      end
    end)
  end

  @spec rev(token) :: :ok | :error
  def rev(token) do
    Agent.get_and_update(__MODULE__, fn state ->
      if token in Map.keys(state.tokens) do
        state = %{state | tokens: Map.delete(state.tokens, token)}
        {:ok, state}
      else
        {:error, state}
      end
    end)
  end

  @spec val(token) :: :ok | :error
  def val(token) do
    Agent.get(__MODULE__, fn state ->
      if token in Map.keys(state.tokens), do: :ok, else: :error
    end)
  end

  defp gen_token([]), do: Enum.random(1..10000)
  defp gen_token(tokens), do: Enum.max(tokens) + 1
end
