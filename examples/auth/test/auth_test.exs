defmodule AuthTest do
  use ExUnit.Case
  use PropCheck

  import PropCheck.StateM

  property "AuthModel" do
    model = AuthModel

    forall cmds <- commands(model) do
      :ok = Auth.start()
      r = {_, _, result} = run_commands(model, cmds)
      Auth.stop()

      (result == :ok)
      |> when_fail(print_report(r, cmds))
      |> aggregate(command_names(cmds))
    end
  end

  property "AuthModelFunctional" do
    model = AuthModelFunctional

    forall cmds <- commands(model) do
      :ok = Auth.start()
      r = {_, _, result} = run_commands(model, cmds)
      Auth.stop()

      (result == :ok)
      |> when_fail(print_report(r, cmds))
      |> aggregate(command_names(cmds))
    end
  end
end
