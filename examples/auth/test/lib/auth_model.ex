defmodule AuthModel do
  import PropCheck.BasicTypes
  use Makina, implemented_by: Auth

  state users: %{} :: %{Auth.user() => Auth.pass()} | %{},
        tokens: [] :: [symbolic(Auth.token())]

  command reg(user :: Auth.user(), pass :: Auth.pass()) :: :ok | :error do
    args user: string(), pass: string()
    valid user not in Map.keys(users)
    next if valid, do: [users: Map.put(users, user, pass)]
    post if valid, do: result == :ok, else: result == :error
  end

  command gen(user :: Auth.user(), pass :: Auth.pass()) :: {:ok, Auth.token()} | :error do
    pre users != %{}

    args let(
           user <- oneof(Map.keys(users)),
           do: [user: user, pass: Map.get(users, user)]
         )

    valid {user, pass} in users
    next if valid, do: [tokens: [symbolic(Kernel.elem(result, 1)) | tokens]]
    post if valid, do: match?({:ok, _}, result)
  end

  command rev(token :: symbolic(Auth.token()) | integer()) :: :ok | :error do
    args token: oneof([pos_integer() | tokens])
    valid token in tokens
    next if valid, do: [tokens: List.delete(tokens, token)]
    post if valid, do: result == :ok, else: result == :error
  end

  command val(token :: symbolic(Auth.token()) | integer()) :: :ok | :error do
    args token: oneof([pos_integer() | tokens])
    valid token in tokens
    post if valid, do: result == :ok
  end

  def string(), do: [char()]
end
