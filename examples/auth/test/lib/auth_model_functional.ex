defmodule AuthModelFunctional do
  use PropCheck
  import PropCheck.BasicTypes

  def initial_state(), do: {%{}, []}

  def command({users, tokens}) do
    if users == %{} do
      {:call, Auth, :reg, [string(), string()]}
    else
      user_pass = let user <- oneof(Map.keys(users)),
                  do: [user, Map.get(users, user)]
      token = oneof([pos_integer()|tokens])
      oneof([
        {:call, Auth, :reg, [string(), string()]},
        {:call, Auth, :reg, user_pass},
        {:call, Auth, :gen, user_pass},
        {:call, Auth, :rev, [token]},
        {:call, Auth, :val, [token]}
      ])
    end
  end

  def precondition({users, _tokens}, call) do
    case call do
      {:call, Auth, :gen, [user, pass]} -> {user, pass} in users
      _ -> true
    end
  end

  def next_state(state = {users, tokens}, result, call) do
    case call do
      {:call, Auth, :reg, [user, pass]}  ->
        if user in users, do: state,
        else: {Map.put(users, user, pass), tokens}
      {:call, Auth, :gen, [user, pass]} ->
        token = {:call, Kernel, :elem, [result, 1]}
        unless {user, pass} in users, do: state,
        else: {users, [token|tokens]}
      {:call, Auth, :rev, [token]} ->
        unless token in tokens, do: state,
        else: {users, List.delete(tokens, token)}
      _ -> state
    end
  end

  def postcondition({users, tokens}, call, result) do
    case call do
      {:call, Auth, :reg, [user, _pass]} ->
        if user in Map.keys(users), do: result == :error,
        else: result == :ok
      {:call, Auth, :gen, [user, pass]} ->
        if {user, pass} in users, do: match?({:ok, _}, result),
        else: result == :error
      {:call, Auth, :rev, [token]} ->
        if token in tokens, do: result == :ok,
        else: result == :error
      {:call, Auth, :val, [token]} ->
        unless token in tokens, do: result == :error,
        else: result == :ok
      _ -> true
    end
  end

  def string(), do: [char()]
end
