# Contents

- [`common`](common) contains the libraries shared by all the subprojects.

- [`counter`](counter) contains a simple counter.

- [`counters`](counters) contains a server that creates new counters.

- [`auth`](auth) contains a very simple authentication API

- [`docs`](docs) contains a very simple document storage API that uses `auth` for authentication.

- [`store`](store) contains a simple stateful service that stores values.

- [`clock`](clock) contains a simple clock.

- [`one_time_password`](one_time_password) contains a one-time-password API.

- [`secret`](secret) contains a server that generates and verifies passwords.

- [`fizz_buzz`](fizz_buzz) contains an implementation of the classical fizzbuzz problem.
