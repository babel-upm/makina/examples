defmodule OneTimePasswordModel do
  use Makina, implemented_by: OneTimePassword

  @type server :: OneTimePassword.otp_server()

  state otp_server: nil :: symbolic(server()) | nil,
        registering: [] :: [String.t()],
        registered: [] :: [String.t()],
        passwords: %{} :: %{String.t() => symbolic(pos_integer())} | %{},
        tries: %{} :: %{String.t() => non_neg_integer()} | %{},
        max_tries: 3 :: non_neg_integer()

  command start_link() :: {:ok, server()} do
    pre is_nil(otp_server)
    next otp_server: Kernel.elem(result, 1) |> symbolic
    weight 1
  end

  command register(server :: symbolic(server()), username :: String.t()) ::
            {:ok, pos_integer()} | :already_registered | :already_registering do
    pre not is_nil(otp_server)
    args server: otp_server, username: username(state)
    valid username not in registering and username not in registered

    post do
      cond do
        username in registering -> result == :already_registering
        username in registered -> result == :already_registered
        valid -> match?({:ok, _otp}, result)
        true -> false
      end
    end

    next do
      if valid do
        password = Kernel.elem(result, 1) |> symbolic()

        [
          passwords: Map.put(passwords, username, password),
          tries: Map.put(tries, username, 1),
          registering: [username | registering]
        ]
      end
    end

    weight 3
  end

  command validate(server :: symbolic(server()), username :: String.t(), password :: symbolic(pos_integer()) | pos_integer()) ::
            :ok | :wrong_otp | :too_many_tries | :not_registering do
    pre not is_nil(otp_server)

    args server: otp_server,
         username: username(state),
         password: password(state)

    post do
      cond do
        username not in registering ->
          result == :not_registering

        Map.get(tries, username) + 1 > max_tries and Map.get(passwords, username) != password ->
          result == :too_many_tries

        Map.get(passwords, username) != password ->
          result == :wrong_otp

        true ->
          result == :ok
      end
    end

    next do
      cond do
        username not in registering ->
          []

        Map.get(tries, username) + 1 > max_tries and Map.get(passwords, username) != password ->
          [registering: List.delete(registering, username)]

        Map.get(passwords, username) != password ->
          [tries: Map.update!(tries, username, &(&1 + 1))]

        true ->
          [
            registering: List.delete(registering, username),
            registered: [username | registered]
          ]
      end
    end

    weight 3
  end

  ##############################################################################

  def username(state) do
    registering = state.registering
    registered = state.registered
    oneof(registering ++ registered ++ [utf8()])
  end

  def password(state) do
    passwords = Map.values(state.passwords)
    oneof(passwords ++ [pos_integer()])
  end
end
