defmodule OneTimePasswordTest do
  use PropCheck
  use ExUnit.Case
  import PropCheck.StateM

  property "OneTimePassword" do
    forall cmds <- commands(OneTimePasswordModel) do
      r = {_, state, result} = run_commands(OneTimePasswordModel, cmds)

      if state.otp_server != nil, do: Agent.stop(state.otp_server)

      (result == :ok)
      |> when_fail(print_report(r, cmds))
      |> aggregate(command_names(cmds))
    end
  end
end
