defmodule OneTimePassword do
  @moduledoc """
  Simplification of a registration system with one-time-password.
  """

  require Agent

  @max_tries 3

  @type otp_server() :: pid()

  @spec start_link() :: {:ok, otp_server()}
  def start_link() do
    Agent.start_link(fn -> %{registering: %{}, registered: MapSet.new()} end)
  end

  @spec register(otp_server(), String.t()) ::
          {:ok, pos_integer()} | :already_registered | :already_registering
  def register(agent, username) do
    Agent.get_and_update(
      agent,
      fn state ->
        if MapSet.member?(state.registered, username) do
          {:already_registered, state}
        else
          case state.registering[username] do
            nil ->
              otp = Enum.random(1..10)
              {{:ok, otp}, %{state | registering: Map.put(state.registering, username, {1, otp})}}

            _ ->
              {:already_registering, state}
          end
        end
      end
    )
  end

  @spec validate(otp_server(), String.t(), pos_integer()) ::
          :ok | :wrong_otp | :too_many_tries | :not_registering
  def validate(agent, username, otp) do
    Agent.get_and_update(
      agent,
      fn state ->
        case state.registering[username] do
          {_tries, ^otp} ->
            {:ok,
             %{
               state
               | registering: Map.delete(state.registering, username),
                 registered: MapSet.put(state.registered, username)
             }}

          {tries, otp} when tries < @max_tries ->
            {:wrong_otp,
             %{
               state
               | registering: Map.put(state.registering, username, {tries + 1, otp})
             }}

          {_tries, _otp} ->
            {:too_many_tries,
             %{
               state
               | registering: Map.delete(state.registering, username)
             }}

          _ ->
            {:not_registering, state}
        end
      end
    )
  end
end
