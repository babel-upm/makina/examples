defmodule Counter do
  require Agent

  @spec start(integer()) :: :error | :ok
  def start(initial_value) do
    case Agent.start_link(fn -> initial_value end, name: __MODULE__) do
      {:ok, _} -> :ok
      _ -> :error
    end
  end

  @spec stop() :: :ok
  def stop() do
    Agent.stop(__MODULE__)
  end

  @spec inc() :: :ok
  def inc() do
    Agent.update(__MODULE__, fn value -> value + 1 end)
  end

  @spec put(integer()) :: :ok
  def put(n) do
    Agent.update(__MODULE__, fn _value -> n end)
  end

  @spec get() :: integer()
  def get() do
    Agent.get(__MODULE__, & &1)
  end
end
