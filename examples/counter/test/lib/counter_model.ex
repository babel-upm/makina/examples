defmodule CounterModel do
  use Makina, implemented_by: Counter

  state value: 0 :: integer()

  invariants greater_than_0: value >= 0

  command inc() :: :ok | :error do
    next value: value + 1
  end

  command put(n :: integer()) :: :ok | :error do
    args n: integer(0, :inf)
    next value: n
  end

  command get() :: integer() do
    post result == value
  end
end
