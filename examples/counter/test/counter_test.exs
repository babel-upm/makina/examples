defmodule CounterTest do
  use ExUnit.Case
  use PropCheck

  import PropCheck.StateM

  property "correctness" do
    initial_value = 0
    model = CounterModel

    forall cmds <- commands(model) do
      assert :ok = Counter.start(initial_value)
      r = {_, _, result} = run_commands(model, cmds)
      Counter.stop()

      (result == :ok)
      |> when_fail(print_report(r, cmds))
      |> aggregate(command_names(cmds))
    end
  end
end
