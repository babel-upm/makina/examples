defmodule Clock do
  @moduledoc """
  Implementation of a trivial digital clock system from the Lamport's
  book Specifying Systems.
  """

  require Agent

  def start_link() do
    Agent.start_link(fn -> %{hr: Enum.random(0..11)} end)
  end

  def stop(clock), do: Agent.stop(clock)

  def time(clock), do: Agent.get(clock, fn %{hr: time} -> 1 + time end)

  def tick(clock), do: Agent.update(clock, fn %{hr: time} -> %{hr: rem(time, 12)} end)
end
