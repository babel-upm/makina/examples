defmodule ClockModel do
  use Makina, implemented_by: Clock

  @type clock() :: pid()

  state clocks: %{} :: %{symbolic(clock()) => nil | integer()} | %{}

  command new() :: clock() do
    call Clock.start_link() |> elem(1)
    next clocks: Map.put(clocks, result, nil)
  end

  command tick(clock :: symbolic(clock())) :: :ok do
    pre clocks != %{}
    args clock: oneof(Map.keys(clocks))
    valid clock in Map.keys(clocks) and not is_nil(clocks[clock])

    next do
      if valid do
        value = Makina.Exports.symbolic(Kernel.+(1, clocks[clock]))
        [clocks: Map.put(state.clocks, clock, value)]
      end
    end
  end

  command time(clock :: symbolic(clock())) :: integer() do
    pre clocks != %{}
    args clock: oneof(Map.keys(clocks))
    valid clock in Map.keys(clocks) and not is_nil(clocks[clock])
    post if valid, do: result == 1 + rem(clocks[clock] - 1, 12)
  end
end
