defmodule ClockTest do
  use PropCheck
  use ExUnit.Case
  import PropCheck.StateM

  property "ClockModel" do
    forall cmds <- commands(ClockModel) do
      trap_exit do
        r = run_commands(ClockModel, cmds)
        {_history, state, result} = r

        for {clock, _} <- state.clocks, do: Clock.stop(clock)

        (result == :ok)
        |> when_fail(print_report(r, cmds))
        |> aggregate(command_names(cmds))
      end
    end
  end
end
