defmodule SecretModel do
  use Makina, implemented_by: Secret

  state secrets: [] :: [%{verifier: symbolic(pid()), secret: symbolic(String.t())}]

  command password() :: {String.t(), pid()} do
    next do
      verifier = Kernel.elem(result, 1) |> symbolic()
      secret = Kernel.elem(result, 0) |> symbolic()
      [secrets: [%{verifier: verifier, secret: secret} | secrets]]
    end
  end

  def passwords(secrets) do
    Enum.map(secrets, fn s -> s[:secret] end)
  end

  def verifiers(secrets) do
    Enum.map(secrets, fn s -> s[:verifier] end)
  end

  command verify_password(password :: symbolic(String.t()), verifier :: symbolic(pid())) do
    pre secrets != []
    args verifier: oneof(verifiers(secrets)), password: oneof(passwords(secrets))

    post do
      entry = Enum.find(secrets, fn m -> m[:verifier] == verifier end)
      equal_secrets = entry[:secret] == password

      case result do
        :ok -> equal_secrets
        :ko -> not equal_secrets
      end
    end
  end
end
