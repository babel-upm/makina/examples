defmodule SecretTest do
  use ExUnit.Case
  use PropCheck
  import PropCheck.StateM

  property "SecretModel" do
    forall cmds <- commands(SecretModel) do
      r = {_, _, result} = run_commands(SecretModel, cmds)

      (result == :ok)
      |> when_fail(print_report(r, cmds))
      |> aggregate(command_names(cmds))
    end
  end
end
