defmodule Secret do
  def server(password) do
    receive do
      {:verify, otherPassword, from} ->
        if password == otherPassword do
          send(from, :ok)
        else
          send(from, :ko)
        end
    end

    server(password)
  end

  defp generate_password do
    passwords = [
      "secreto",
      "secret",
      "hemlig",
      "skyddad",
      "protegido",
      "protected"
    ]

    nth = Enum.random(0..(length(passwords) - 1))
    Enum.at(passwords, nth)
  end

  def password() do
    password = generate_password()
    {password, spawn(fn -> server(password) end)}
  end

  def verify_password(password, oracle) do
    send(oracle, {:verify, password, self()})

    receive do
      msg -> msg
    end
  end
end
