defmodule Docs do
  require Agent

  @type server() :: pid()
  @type user() :: String.t()
  @type token() :: Auth.token()
  @type key() :: integer()
  @type doc() :: any()

  @spec start() :: :ok | :error
  def start() do
    :ok = Auth.start()
    {:ok, _} = Agent.start_link(fn -> %{} end, name: __MODULE__)
    :ok
  end

  @spec stop() :: :ok
  def stop() do
    :ok = Auth.stop()
    :ok = Agent.stop(__MODULE__)
    :ok
  end

  @spec put(token, key, doc) :: :ok | :error
  def put(token, key, doc) do
    with :ok <- Auth.val(token) do
      Agent.update(__MODULE__, fn state -> Map.put(state, key, doc) end)
    else
      _ -> :error
    end
  end

  @spec del(token, key) :: :ok | :error
  def del(token, key) do
    with :ok <- Auth.val(token) do
      Agent.get_and_update(__MODULE__, fn state ->
        if key in Map.keys(state) do
          {:ok, Map.delete(state, key)}
        else
          {:error, state}
        end
      end)
    else
      _ -> :error
    end
  end

  @spec get(key) :: {:ok, doc} | :error
  def get(key) do
    Agent.get(__MODULE__, fn state -> Map.fetch(state, key) end)
  end
end
