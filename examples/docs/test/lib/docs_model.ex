defmodule DocsModel do
  import PropCheck.BasicTypes

  use Makina,
    extends: AuthModel,
    implemented_by: Docs,
    where: [val: :put, val: :del]

  state docs: %{} :: %{Docs.key() => Docs.doc()} | %{}

  command put(key :: Docs.key(), doc :: Docs.doc()) :: :ok | :error do
    args key: integer(), doc: string()
    next if valid, do: [docs: Map.put(docs, key, doc)]
  end

  command del(key :: Docs.key()) :: :ok | :error do
    args key: oneof([integer() | Map.keys(docs)])
    valid key in Map.keys(docs)
    next if valid, do: [docs: Map.delete(docs, key)]
  end

  command get(key :: Docs.key()) :: {:ok, Docs.doc()} | :error do
    args key: oneof([integer() | Map.keys(docs)])
    post result == Map.fetch(docs, key)
  end

  def string(), do: [char()]
end
