defmodule DocsComp.Get do
  use Makina, extends: DocsComp.Base, implemented_by: Docs

  command get(key :: Docs.key()) :: {:ok, Docs.doc()} | :error do
    args key: oneof([integer() | Map.keys(docs)])
    post result == Map.fetch(docs, key)
  end
end
