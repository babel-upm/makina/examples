defmodule DocsComp.Put do
  use Makina, extends: DocsComp.Base, where: [val: :put], implemented_by: Docs

  command put(key :: Docs.key(), doc :: Docs.doc()) :: :ok | :error do
    args key: integer(), doc: [char()]
    next if valid, do: [docs: Map.put(docs, key, doc)]
  end
end
