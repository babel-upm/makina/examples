defmodule DocsComp.Base do
  import PropCheck.BasicTypes
  use Makina, extends: AuthModel
  state docs: %{} :: %{Docs.key() => Docs.doc()} | %{}
end
