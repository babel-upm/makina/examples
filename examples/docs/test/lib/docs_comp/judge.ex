defmodule DocsComp.Judge do
  use Makina, extends: [DocsComp.Put, DocsComp.Del, DocsComp.Get]
  invariants uniq_tokens: Enum.uniq(tokens) == tokens
end
