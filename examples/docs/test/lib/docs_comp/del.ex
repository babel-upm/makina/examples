defmodule DocsComp.Del do
  use Makina, extends: DocsComp.Base, where: [val: :del]

  command del(key :: Docs.key()) :: :ok | :error do
    args key: oneof([integer() | Map.keys(docs)])
    valid key in Map.keys(docs)
    next if valid, do: [docs: Map.delete(docs, key)]
  end
end
