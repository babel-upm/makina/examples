defmodule DocsTest do
  use ExUnit.Case
  use PropCheck

  import PropCheck.StateM

  property "correctness via extension" do
    model = DocsModel

    forall cmds <- commands(model) do
      :ok = Docs.start()
      r = {_, _, result} = run_commands(model, cmds)
      Docs.stop()

      (result == :ok)
      |> when_fail(print_report(r, cmds))
      |> aggregate(command_names(cmds))
    end
  end

  property "correctness via parallel composition" do
    model = DocsComp.Judge

    forall cmds <- commands(model) do
      :ok = Docs.start()
      r = {_, _, result} = run_commands(model, cmds)
      Docs.stop()

      (result == :ok)
      |> when_fail(print_report(r, cmds))
      |> aggregate(command_names(cmds))
    end
  end
end
