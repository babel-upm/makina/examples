defmodule Common.MixProject do
  use Mix.Project

  def project do
    [
      app: :common,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:makina, git: "git@gitlab.com:babel-upm/makina/makina.git", branch: "develop"},
      {:corsa, git: "git@gitlab.com:babel-upm/corsa/corsa.git", branch: "develop"},
      {:propcheck, github: "alfert/propcheck", ref: "c564e89d3873caf9c6bf64a2af4bb3890e24ecf1", only: [:test, :dev]}, # fixes register_test warning
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false}
    ]
  end
end
